/* includes.h
 *
 * Created: 31/03/2016 3:29:57 PM
 *  Author: Lachlan Fitzpatrick
 */

// Clock speed
#define F_CPU 16000000UL

// Baud rate
#define BAUD_RATE 57600
#define UBRR_VALUE (F_CPU / 16 / BAUD_RATE) - 1

// Boolean
#define bool char
#define false 0
#define true 1

// Structure for holding all the motor data
typedef struct motor_data {
    // Motor variables
    char left_direction;
    char left_speed;
    char right_direction;
    char right_speed;
    // Servo variables
    char servo_direction;
} motor_data_t;
