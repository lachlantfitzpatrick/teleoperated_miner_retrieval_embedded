/*
 * RemoteMotorControl.c
 *
 * The main file that runs the program. Constantly loops checking for control 
 * data and responds accordingly.
 *
 * Created: 31/03/2016 3:04:09 PM
 * Authors: Lachlan Fitzpatrick, Tom Hines
 */

#include "includes.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

#include "motors.h"
#include "servo.h"
#include "serial.h"
#include "timer_control.h"

// Input buffer
char rx_message[RX_BUFFER_LENGTH];

// Main
void main(void) {
    // Disable interrupts
    cli();

    // Initialise
    motors_init();
    servo_init();
    serial_init();
    timer_control_init();

    // Set up status LED
    DDRB |= (1 << PB3);

    // Enable interrupts
    sei();

    // The received motor data
    motor_data_t received_motor_data = {
        .left_direction = 0,
        .left_speed = 50,
        .right_direction = 0,
        .right_speed = 0,
        .servo_direction = 0
    };
	
    set_motor_data(&received_motor_data);

    // Main loop
    while (1) {
        // Check for serial line
        if (serial_rx_ln_ready()) {
            // Get line
            memset(rx_message, '\0', RX_BUFFER_LENGTH);
            serial_rx_ln_pop(rx_message);
            // Check if message from ESP
            if (rx_message[0] == 'I' && rx_message[6] == 'I') {
                // This is a message from ESP
                received_motor_data.left_direction = rx_message[1];
                received_motor_data.left_speed = rx_message[2];
                received_motor_data.right_direction = rx_message[3];
                received_motor_data.right_speed = rx_message[4];
                received_motor_data.servo_direction = rx_message[5];
                // Reset control ticks.
                reset_control_tick();
                // Set the motor data.
                set_motor_data(&received_motor_data);
            }
        }

        // Handle timer updates.
        system_update();

    }
}
