/* motors.c
 *
 * Created: 1/04/2016 3:06:04 PM
 *  Author: Liam McMahon
 */

#include "includes.h"

#include <avr/io.h>

#include "motors.h"

// Private defines
// Motor ports and registers (maybe these are not necessary)
#define MOTOR_SPEED_PORT PORTB
#define MOTOR_DIRECTION_PORT PORTD
#define MOTOR_LEFT_SPEED OCR1A
#define MOTOR_LEFT_SPEED_PIN PB1
#define MOTOR_LEFT_BACKWARD PD4
#define MOTOR_LEFT_FORWARD PD7
#define MOTOR_RIGHT_SPEED OCR1B
#define MOTOR_RIGHT_SPEED_PIN PB2
#define MOTOR_RIGHT_BACKWARD PD3
#define MOTOR_RIGHT_FORWARD PD2

// Private macros
// Private variables
// Function definitions
// Set up the motors
void motors_init(void) {
    // Configure motors PWM
    TCCR1A |= (1 << WGM11) | (1 << WGM10);
    TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
    TCCR1B |= (1 << CS11);
    TCNT1 = 0;

    // Set pin modes
    DDRB |= (1 << MOTOR_LEFT_SPEED_PIN) |
        (1 << MOTOR_RIGHT_SPEED_PIN);
    DDRD |= (1 << MOTOR_LEFT_FORWARD) | (1 << MOTOR_LEFT_BACKWARD) |
        (1 << MOTOR_RIGHT_FORWARD) | (1 << MOTOR_RIGHT_BACKWARD);

    // Clear state
    motors_directions_set(FORWARD, FORWARD);
    motors_speeds_set(0, 0);
}

// Set the motor directions
void motors_directions_set(char left, char right) {
    // Clear motor directions
    MOTOR_DIRECTION_PORT &= ~(
        (1 << MOTOR_LEFT_FORWARD) | (1 << MOTOR_LEFT_BACKWARD) |
        (1 << MOTOR_RIGHT_FORWARD) | (1 << MOTOR_RIGHT_BACKWARD));

    // Set motor directions. No need for stop as this would be the exception
    MOTOR_DIRECTION_PORT |= ((left == FORWARD) << MOTOR_LEFT_FORWARD) |
        ((left == BACKWARD) << MOTOR_LEFT_BACKWARD);
    MOTOR_DIRECTION_PORT |= ((right == FORWARD) << MOTOR_RIGHT_FORWARD) |
        ((right == BACKWARD) << MOTOR_RIGHT_BACKWARD);
}

// Set the motor speeds
void motors_speeds_set(int left, int right) {
    // Set duty cycles
    MOTOR_LEFT_SPEED = (left * 1023) / MAX_SPEED;
    MOTOR_RIGHT_SPEED = (right * 1023) / MAX_SPEED;
}
