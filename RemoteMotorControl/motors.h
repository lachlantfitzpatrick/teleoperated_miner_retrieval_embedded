/* motors.h
 *
 * Created: 1/04/2016 3:12:14 PM
 *  Author: Liam McMahon
 */

#ifndef MOTORS_H_
#define MOTORS_H_

// Public defines
// Directions
#define STOP 1
#define BACKWARD 2
#define FORWARD 3
// Max speed of the motors
#define MAX_SPEED 100
#define MIN_SPEED 1

// Public macros
// Public variables
// Function prototypes
void motors_init(void);
void motors_speeds_set(int left, int right);
void motors_directions_set(char left, char right);

#endif
