/* serial.c
 *
 * Handles sending and receiving data by serial. All actual data sending and
 * receiving is done in interrupts and the data itself is stored here.
 *
 * Created: 27/04/2016 5:25:17 PM
 *  Author: Tom Hines
 */

#include "includes.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "serial.h"

// Private defines
// Private macros
// Private variables
char format_string[FORMAT_LENGTH];

// Transmit state
uint8_t tx_index_write;
volatile uint8_t tx_index_read;
volatile char tx_buffer[TX_BUFFER_LENGTH];

// Receive state
uint8_t rx_index_read;
volatile uint8_t rx_index_write;
volatile char rx_buffer[RX_BUFFER_LENGTH];
volatile uint8_t rx_ln_start;
volatile uint8_t rx_ln_end;
volatile bool rx_ln_ready;

// Function definitions
// Set up serial registers
void serial_init(void) {
    // Set BAUD rate
    UBRR0 = UBRR_VALUE;

    // Enable transmitter
    UCSR0B |= (1 << TXCIE0) | (1 << TXEN0);

    // Enable receiver
    UCSR0B |= (1 << RXCIE0) | (1 << RXEN0);

    // Set frame format: 8 data and 2 stop bits
    UCSR0C |= (1 << USBS0) | (3 << UCSZ00);

    // Clear Tx state
    tx_index_write = 0;
    tx_index_read = 0;

    // Clear Rx state
    rx_index_write = 0;
    rx_index_read = 0;
    rx_ln_start = 0;
    rx_ln_end = 0;
    rx_ln_ready = 0;
}

// Add a string to the transmit buffer
void serial_send(char *message, uint8_t length) {
    uint8_t index_copy = 0;
    // Until the end of the string
    while (message[index_copy] != '\0') {
        // Copy the character
        tx_buffer[tx_index_write] = message[index_copy];
        // Increment write index
        tx_index_write++;
        if (tx_index_write >= TX_BUFFER_LENGTH) {
            tx_index_write = 0;
        }
        // Increment copy index
        index_copy++;
        // Check if at end of message
        if (length != 0 && index_copy >= length) {
            break;
        }
    }
    // Start transmission if stopped
    if (UCSR0A & (1 << UDRE0)) {
        UDR0 = tx_buffer[tx_index_read];
        // Increment read index
        tx_index_read++;
        if (tx_index_read >= TX_BUFFER_LENGTH) {
            tx_index_read = 0;
        }
    }
}

// Add a line ending to the transmit buffer
void serial_state(void) {
    // serial_printf("ramend@%d\r\n", RAMEND);
    // Buffer starts
    // serial_printf("txb@%d\r\n", tx_buffer);
    // serial_printf("rxb@%d\r\n", rx_buffer);
    // Indexes
    serial_printf("txr=%d/%d\r\n\0", tx_index_read, TX_BUFFER_LENGTH);
    serial_printf("txw=%d/%d\r\n\0", tx_index_write, TX_BUFFER_LENGTH);
    serial_printf("rxr=%d/%d\r\n\0", rx_index_read, RX_BUFFER_LENGTH);
    serial_printf("rxw=%d/%d\r\n\0", rx_index_write, RX_BUFFER_LENGTH);
}

void serial_endl(void) {
    serial_send("\r\n", 2);
}

// Add a string and line ending to the transmit buffer
void serial_sendln(char *message) {
    serial_send(message, 0);
    serial_endl();
}

// Format a string then add it to the transmit buffer
void serial_printf(const char *format, ...) {
    // Get arguments
    va_list args;
    va_start(args, format);
        // Do formatting
        vsprintf(format_string, format, args);
    va_end(args);
    // Buffer the result
    serial_send(format_string, 0);
}

// Add the terminal clear string to the transmit buffer
void serial_clear() {
    serial_send("\033[2J\033[1;1H", 10);
}

// Get the number of unread characters in the receive buffer
uint8_t serial_rx_count(void) {
    return (RX_BUFFER_LENGTH +
        rx_index_write - rx_index_read) % RX_BUFFER_LENGTH;
}

// Get the next unread character in the receive buffer
char serial_rx_pop(void) {
    char rx_character = rx_buffer[rx_index_read];
    // If more characters in buffer
    if (rx_index_read != rx_index_write) {
        // Increment read index
        rx_index_read++;
        if (rx_index_read >= RX_BUFFER_LENGTH) {
            rx_index_read = 0;
        }
    }
    return rx_character;
}

// Get the number of unread lines in the receive buffer
bool serial_rx_ln_ready(void) {
    return rx_ln_ready;
}

// Pop the next unread line from the receive buffer
void serial_rx_ln_pop(char *message) {
    // Copy from buffer to message
    uint8_t index_copy = 0;
    while (rx_ln_start != rx_ln_end) {
        message[index_copy] = rx_buffer[rx_ln_start];
        // Increment read index
        rx_ln_start++;
        if (rx_ln_start >= RX_BUFFER_LENGTH) {
            rx_ln_start = 0;
        }
        // Increment copy index
        index_copy++;
    }
    // Copy string terminator
    message[index_copy] = '\0';

    // Clear line ready flag
    rx_ln_ready = false;

    // Print the message
    // serial_printf("rx \"%s\"\r\n", message);
}

// Interrupts
ISR(USART_TX_vect) {
    // If there is something to write
    if (tx_index_read != tx_index_write) {
        // Write it
        UDR0 = tx_buffer[tx_index_read];
        // Increment read index
        tx_index_read++;
        if (tx_index_read >= TX_BUFFER_LENGTH) {
            tx_index_read = 0;
        }
    }
}

ISR(USART_RX_vect) {
    // Store character
    char rx_character = UDR0;
    // Check if end of line
    if (rx_character == '\r' || rx_character == '\n') {
        // Check if line was empty
        if ((rx_ln_end + 1) % RX_BUFFER_LENGTH == rx_index_write) {
            // Skip double line ending
            rx_index_write--;
            // NOTE: the line will also be skipped if it has a length equal to
            // RX_BUFFER_LENGTH or something like that, soz
        } else {
            // Copy line ending
            rx_buffer[rx_index_write] = '\n';
            // Move start to just after old end
            rx_ln_start = (rx_ln_end + 1) % RX_BUFFER_LENGTH;
            // Move end to here
            rx_ln_end = rx_index_write;
            // Set line ready flag
            rx_ln_ready = true;
        }
    } else {
        // Copy character
        rx_buffer[rx_index_write] = rx_character;
    }
    // Increment write index
    rx_index_write++;
    if (rx_index_write >= RX_BUFFER_LENGTH) {
        rx_index_write = 0;
    }
}
