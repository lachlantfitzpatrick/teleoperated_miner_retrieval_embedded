/* serial.h
 *
 * Handles sending and receiving data by serial. All actual data sending and
 * receiving is done in interrupts and the data itself is stored here.
 *
 * Created: 27/04/2016 5:25:17 PM
 *  Author: Tom Hines
 */

#ifndef SERIAL_H_
#define SERIAL_H_

// Public defines
#define TX_BUFFER_LENGTH 256
#define RX_BUFFER_LENGTH 256
#define FORMAT_LENGTH 256

// Public macros
// Public variables

// Function prototypes
void serial_init(void);
void serial_send(char *message, uint8_t length);
void serial_state(void);
void serial_endl(void);
void serial_sendln(char *message);
void serial_printf(const char *format, ...);
void serial_clear(void);
uint8_t serial_rx_count(void);
char serial_rx_pop(void);
bool serial_rx_ln_ready(void);
void serial_rx_ln_pop(char *message);

#endif
