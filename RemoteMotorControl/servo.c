/* servo.c
 *
 * Created: 27/04/2016 6:15:27 PM
 *  Author: Tom Hines
 */

#include "includes.h"

#include <avr/io.h>

#include "servo.h"

// Private defines
// Servo ports and registers (maybe these are not necessary)
#define SERVO OCR0A

// Private macros

// Private variables.
// Angle is between 0 and 100 (percent of max).
char angle = 0;
// Function definitions
// Set up servo
void servo_init(void) {
    // Configure servo PWM
    // Try to work out what these mean:
    TCCR0A |= (1 << COM0A1) | (0 << COM0A0) | (1 << WGM01) | (1 << WGM00);
    TCCR0B |= (1 << CS02) | (0 << CS01) | (1 << CS00);

    // Set pin modes
    DDRD |= (1 << PD6);

    //  Clear state
    servo_set(100);
}

// Move servo to angle
void servo_set(char new_angle) {
    SERVO = (float) (17 + new_angle * 0.33);
	angle = new_angle;
}

// Increase the angle
void servo_angle_up(void) {
    angle++;
    // Check we haven't gone too far
    if(angle > MAX_ANGLE){
        angle = MAX_ANGLE;
    }
    // Set the new position
    servo_set(angle);
}

// Increase the angle
void servo_angle_down(void) {
    angle--;
    // Check we haven't gone too far
    if(angle < MIN_ANGLE){
        angle = MIN_ANGLE;
    }
    // Set the new position
    servo_set(angle);
}
