/* servo.h
 *
 * Created: 27/04/2016 6:15:27 PM
 *  Author: Tom Hines
 */

#ifndef SERVO_H_
#define SERVO_H_

// Public defines
// Angle limits
#define MAX_ANGLE 100
#define MIN_ANGLE 0
// Servo directions
#define STOP 1
#define DOWN 2
#define UP 3

// Public macros
// Public variables
// Function prototypes
void servo_init(void);
void servo_set(char angle);
void servo_angle_up(void);
void servo_angle_down(void);

#endif
