/*
 * timer_control.c
 *
 * Handles all timer based control of the system. This includes ramping the
 * motors, moving the servo and responding to the communications.
 *
 * Created: 2/05/2016 4:48:27 PM
 * Author: Lachlan Fitzpatrick
 */

#include "includes.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>

#include "timer_control.h"
#include "motors.h"
#include "servo.h"

// Private prototypes
void accelerate_motor(char* target_direction, char* current_direction,
	char* target_speed, char* current_speed);
void accelerate_motors(void);
void timer2_init(void);

// Private variables.
// Timer 2 ticks used to monitor control data arrivals.
uint8_t controlTick = 0;
// Timer 2 ticks used to accelerate the motors.
uint8_t motorTick = 0;

// Motor data.
motor_data_t* target_motor_data;
motor_data_t* current_motor_data;

/*
 * Initialises Timer 2 and allocates memory for the motor data.
 */
void timer_control_init(void) {
	// Initialise timer
	timer2_init();
	// Allocate memory for motor data
	target_motor_data = malloc(sizeof(motor_data_t));
	current_motor_data = malloc(sizeof(motor_data_t));
}

/*
 * Initialises Timer 2 (the 8 bit timer). With scalings output compare has a
 * frequency of ~61Hz.
 */
void timer2_init(void) {
	// Set the compare match register to match every ~60Hz (1/(FOSC/1024))
	OCR2A = 255;
	// Compare match mode.
	TCCR2A |= (1 << WGM21);
	// Set CS10 and CS12 bits for 1024 prescaler.
	TCCR2B |= (1 << CS20) | (1 << CS21) | (1 << CS22);
	// Enable the timer compare interrupt.
	TIMSK2 |= (1 << OCIE2A);
}

/*
 * Resets the control ticks.
 */
void reset_control_tick(void) {
    controlTick = 0;
}

/*
 * Set motor data.
 */
void set_motor_data(motor_data_t *new_motor_data) {
    target_motor_data = new_motor_data;
    // The current servo direction should automatically be the target
    current_motor_data->servo_direction = target_motor_data->servo_direction;
}

/*
 * Updates all the outputs from the timer based control.
 */
void system_update(void) {
    // Check whether control data has been received, if not stop the motors.
    if ((uint32_t) (controlTick * 1000) / TIMER2_FREQ > CONTROL_RATE) {
        // Set the motor speeds to zero.
        target_motor_data->left_speed = 0;
        target_motor_data->right_speed = 0;
        target_motor_data->servo_direction = 0;
        // Reset control ticks.
        controlTick = 0;
    }
}

/*
 * Accelerates the motors towards the target motor value. 
 */
void accelerate_motors(void) {
	// Accelerate left motor
	accelerate_motor(&(target_motor_data->left_direction), 
		&(current_motor_data->left_direction), &(target_motor_data->left_speed),
		&(current_motor_data->left_speed));
	// Accelerate right motor
	accelerate_motor(&(target_motor_data->right_direction),
		&(current_motor_data->right_direction), &(target_motor_data->right_speed),
		&(current_motor_data->right_speed));
		
	// Set the motors.
	motors_directions_set(current_motor_data->left_direction, 
		current_motor_data->right_direction);
	motors_speeds_set(current_motor_data->left_speed, 
		current_motor_data->right_speed);
}

/*
 * Accelerates an individual motor.
 */
void accelerate_motor(char* target_direction, char* current_direction, 
	char* target_speed, char* current_speed){
	// Check the direction
	if(*current_direction != *target_direction || *target_direction == STOP){
		*current_speed -= ACCELERATION_INCREMENT;
		// Check whether the last decrement overflowed
		if(*current_speed < MIN_SPEED){
			// Swap direction and set speed to zero
			*current_direction = *target_direction;
			*current_speed = 0;
		}
	}
	// Do nothing for same speeds
	else if(*current_speed == *target_speed){}
	// Check if we're greater than the target speed plus an increment
	else if(*current_speed > *target_speed + ACCELERATION_INCREMENT){
		*current_speed -= ACCELERATION_INCREMENT;
	}
	// Check if we're less than the target speed minus an increment
	else if(*current_speed < *target_speed - ACCELERATION_INCREMENT){
		*current_speed += ACCELERATION_INCREMENT;
	}
	// As a security set the motor speed to the target
	else{
		*current_speed = *target_speed;
	}
}

/*
Handles not receiving control data and accelerating the motors.
*/
ISR(TIMER2_COMPA_vect) {
    // Increment the control tick
    controlTick++;

	// Increment the motor tick
	motorTick++;
    // Handle the tick
	if(motorTick > MOTOR_INCREMENT_TICKS){
		accelerate_motors();
		motorTick = 0;
	}

    // Move the servo
    switch(target_motor_data->servo_direction){
        case STOP;
            break;
        case UP:
            servo_angle_up();
            break;
        case DOWN:
            servo_angle_down();
            break;
    }
}
