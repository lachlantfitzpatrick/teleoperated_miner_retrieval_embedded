/*
 * timer_control.h
 *
 * Created: 2/05/2016 4:48:42 PM
 *  Author: Lachlan Fitzpatrick
 */


#ifndef TIMER_CONTROL_H_
#define TIMER_CONTROL_H_

// Public defines
// The expected wait time between control packages
#define CONTROL_RATE 600
// Approximate frequency of Timer2 OCR in Hertz
#define TIMER2_FREQ 60

// Acceleration increment
#define ACCELERATION_INCREMENT 5
// Max acceleration time (milliseconds)
#define MAX_ACCELERATION_TIME (uint32_t) 1000
// Frequency of motor increment (milliseconds)
#define MOTOR_INCREMENT_PERIOD (uint32_t) MAX_ACCELERATION_TIME/(2*ACCELERATION_INCREMENT)
// Number of motor ticks
#define MOTOR_INCREMENT_TICKS (uint32_t) MOTOR_INCREMENT_PERIOD*TIMER2_FREQ/1000

// Prototypes
void system_update(void);
void reset_control_tick(void);
void set_motor_data(motor_data_t* new_motor_data);
void timer_control_init(void);

#endif /* TIMER_CONTROL_H_ */